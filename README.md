# Wardrobify

Team:

* Corissa W. - Shoes
* Austin B. - Hats

## Design

## Shoes microservice

The Shoes microservice integrates with the Wardrobe microservice's Bin model through polling, utilizing the BinVO model, to enable users to perform actions such as creating, deleting, and viewing a list of shoes along with their details.

## Hats microservice

The Hats microservice uses LocationVO(using polling from Wardrobe API location model)
allowing the end user to create/delete hats and view a list of the hats and their details.
