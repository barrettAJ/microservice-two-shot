import React, {useState, useEffect} from 'react'

export default function ShoesList(props){
    const [alertVisible, setAlertVisible] = useState(localStorage.getItem('alertVisible') === 'true');
    useEffect(() => {
        if (alertVisible) {
            setTimeout(() => {
                setAlertVisible(false);
                localStorage.setItem('alertVisible', 'false');
            }, 2000);
        }
    }, [alertVisible]);

    if(!props.shoes){
        return null;
    }


    function refreshPage() {
        window.location.reload(false);
    }
    async function deleteShoe(id) {
        const hatUrl = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            localStorage.setItem('alertVisible', 'true');
            refreshPage();
        }
    }

    return(
        <div className="container ">
            {alertVisible && (
                <div className="alert alert-success" role="alert" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                    Shoe successfully deleted!
                </div>
            )}
            <div className="my-5 card">
                <table className="table table-striped">
                    <thead className="bg-dark text-light">
                        <tr>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col"></th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Manufacturer</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Model</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Color</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Bin</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Picture</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.shoes.map((shoe, index) => (
                            <tr key={shoe.id}>
                                <th scope="row" style={{ textAlign: 'center', verticalAlign: 'middle' }}>{index + 1}</th>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{shoe.manufacturer}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{shoe.model_name}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{shoe.color}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{shoe.bin_number}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}><img src={ shoe.picture_URL } height="100" width="100" className="img-thumbnail"/></td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                                    <button className="btn btn-danger" onClick={() => {deleteShoe(shoe.id)}}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
