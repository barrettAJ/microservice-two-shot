import React, {useState, useEffect} from 'react'

function HatsForm(){
    const[fabric, setFabric] = useState('');
    const[styleName, setStyleName] = useState('');
    const[color, setColor] = useState('');
    const[url, setURL] = useState('');
    const[location, setLocation] = useState('');
    const[locations, setLocations] = useState([]);

    //handlers
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleURLChange = (event) => {
        const value = event.target.value;
        setURL(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async()=>{
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.image_url = url;
        data.location = location;
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig= {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(hatsUrl, fetchConfig);

        if(response.ok){
            const newHat = await response.json();
            setFabric('');
            setStyleName('');
            setColor('');
            setLocation('');
            setURL('');
        }
    }


    useEffect(() =>{
        fetchData();
    }, []);

    return (
        <div className="container ">
            <div className="my-5">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-hat-form">
                                    <h1 className="card-title text-center">New hat</h1>
                                    <div className=" row mb-3">
                                        <div className="col">
                                            <select onChange={handleLocationChange} name="location" id="location" required value={location} className="form-select mb-3">
                                                <option value="">Choose a location</option>
                                                {locations.map((location) =>(
                                                <option key={location.href} value={location.href}>{location.closet_name}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleFabricChange} required placeholder="Fabric name" type="text" id="fabric" name="fabric" className="form-control" value={fabric}/>
                                                <label htmlFor="fabric">Fabric</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleStyleNameChange} required placeholder="Style" type="text" id="style" name="style" className="form-control" value={styleName}/>
                                                <label htmlFor="style">Style</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleColorChange} required placeholder="Color" type="text" id="color" name="color" className="form-control" value={color}/>
                                                <label htmlFor="color">Color</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleURLChange} required placeholder="image_url" type="url" id="image_url" name="image_url" className="form-control" value={url}/>
                                                <label htmlFor="url">Image URL</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-outline-primary btn-lg btn-block">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default HatsForm;
