import React, {useState, useEffect} from 'react'

export default function HatsList(props){
    const [alertVisible, setAlertVisible] = useState(localStorage.getItem('alertVisible') === 'true');

    useEffect(() => {
        if (alertVisible) {
            setTimeout(() => {
                setAlertVisible(false);
                localStorage.setItem('alertVisible', 'false');
            }, 2000);
        }
    }, [alertVisible]);

    if(!props.hats){
        return null;
    }

    function refreshPage() {
        window.location.reload(false);
    }
    async function deleteHat(id) {
        const hatUrl = `http://localhost:8090/api/hats/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            localStorage.setItem('alertVisible', 'true');
            refreshPage();
        }
    }

    return(
        <div className="container">
            {alertVisible && (
                <div className="alert alert-success" role="alert" style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                    Hat successfully deleted!
                </div>
            )}
            <div className="my-5 card">
                <table className="table table-striped">
                    <thead className="bg-dark text-light">
                        <tr>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col"></th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Fabric</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Style</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Color</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Location</th>
                            <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Picture</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.hats.map((hat, index) => (
                            <tr key={hat.id}>
                                <th scope="row" style={{ textAlign: 'center', verticalAlign: 'middle' }}>{index + 1}</th>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{hat.fabric}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{hat.style_name}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{hat.color}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{hat.location}</td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}><img src={ hat.image_url } height="100" width="100" className="img-thumbnail"/></td>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
                                    <button className="btn btn-danger" onClick={() => {deleteHat(hat.id)}}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}
