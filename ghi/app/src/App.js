import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsForm from './HatsForm';
import HatsList from './HatsList';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';

function App({hats, shoes}) {
  if(hats === undefined || shoes === undefined){
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='hats'>
            <Route path='' element={<HatsList hats={hats}/>}/>
            <Route path='new' element={<HatsForm />}/>
          </Route>
          <Route path='shoes'>
            <Route path=''element={<ShoesList shoes={shoes}/>}/>
            <Route path='new' element={<ShoesForm />}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
