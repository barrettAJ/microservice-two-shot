import React, {useState, useEffect} from 'react'

function ShoesForm(){
    const[manufacturer, setManufacturer] = useState('');
    const[modelName, setModelName] = useState('');
    const[color, setColor] = useState('');
    const[url, setURL] = useState('');
    const[bin, setBin] = useState('');
    const[bins, setBins] = useState([]);


    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleURLChange = (event) => {
        const value = event.target.value;
        setURL(value);
    }
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    const fetchData = async()=>{
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setBins(data.bins);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_URL = url;
        data.bin_number = bin;
        const shoesUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig= {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoesUrl, fetchConfig);

        if(response.ok){
            const newShoe = await response.json();
            setManufacturer('');
            setModelName('');
            setColor('');
            setBin('');
            setURL('')
        }
    }


    useEffect(() =>{
        fetchData();
    }, []);

    return (
        <div className="container ">
            <div className="my-5">
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="card shadow">
                            <div className="card-body">
                                <form onSubmit={handleSubmit} id="create-hat-form">
                                    <h1 className="card-title text-center">New shoe</h1>
                                    <div className=" row mb-3">
                                        <div className="col">
                                            <select onChange={handleBinChange} name="bin" id="bin" required value={bin} className="form-select mb-3">
                                                <option value="">Choose a bin</option>
                                                {bins.map((bin) =>(
                                                <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                                                ))}
                                            </select>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                            <input onChange={handleManufacturerChange} required placeholder="Manufacturer" type="text" id="manufacturer" name="manufacturer" className="form-control" value={manufacturer}/>
                                            <label htmlFor="manufacturer">Manufacturer</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleModelNameChange} required placeholder="model_name" type="text" id="model_name" name="model_name" className="form-control" value={modelName}/>
                                                <label htmlFor="model_name">Model Name</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleColorChange} required placeholder="Color" type="text" id="color" name="color" className="form-control" value={color}/>
                                                <label htmlFor="color">Color</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-3">
                                        <div className="col">
                                            <div className="form-floating mb-3">
                                                <input onChange={handleURLChange} required placeholder="image_url" type="url" id="image_url" name="image_url" className="form-control" value={url}/>
                                                <label htmlFor="url">Image URL</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center">
                                        <button className="btn btn-outline-primary btn-lg btn-block">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}


export default ShoesForm;
