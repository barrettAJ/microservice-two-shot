from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import LocationVO, Hats


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "shelf_number",
        "section_number",
        "import_href",
    ]


class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        'id',
        "fabric",
        "style_name",
        "color",
        "location",
        "image_url",

    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "image_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(['GET', 'POST'])
def list_hats(request, location_vo_id=None):
    if request.method == 'GET':
        if location_vo_id is not None:
            location_href = f"/api/locations/{location_vo_id}/"
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {'hats': hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'message': 'Invalid location ID'},
                status=400,
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            {'hat': hat},
            encoder=HatDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def hat_detail(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Hats.objects.filter(id=pk).update(**content)
        hat = Hats.objects.get(id=id)
        return JsonResponse({"hat": hat}, encoder=HatDetailEncoder, safe=False)
